# ggplot2


## Introdução

As sugestões de estilo usadas no `+` para separar as camadas do ggplot2 são muito semelhantes às 
de `%>%` pipelines.

## Espaço em branco

O `+` deve sempre ter um espaço à sua frente e deve ser seguido por uma nova linha. Isso é verdade
mesmo se seu gráfico tiver apenas duas camadas. Após o primeiro passo, cada linha deve ser recuada 
por dois espaços.

Se você estiver criando um ggplot a partir de um pipeline dplyr, deve haver apenas um nível de recuo.

```{r eval=FALSE}
# Good
iris %>%
  filter(Species == "setosa") %>%
  ggplot(aes(x = Sepal.Width, y = Sepal.Length)) +
  geom_point()

# Bad
iris %>%
  filter(Species == "setosa") %>%
  ggplot(aes(x = Sepal.Width, y = Sepal.Length)) +
    geom_point()

# Bad
iris %>%
  filter(Species == "setosa") %>%
  ggplot(aes(x = Sepal.Width, y = Sepal.Length)) + geom_point()
```


## Linhas longas

Se os argumentos para uma camada ggplot2 não couberem em uma linha, coloque cada argumento em sua 
própria linha e indente.

Use no máximo três argumentos por linhas. Caso os três argumentos sejam grandes, opte por colocar cada
um em uma linha.

```{r, eval = FALSE}
# Good
ggplot(aes(x = Sepal.Width, y = Sepal.Length, color = Species)) +
  geom_point() +
  labs(
    x = "Sepal width, in cm",
    y = "Sepal length, in cm",
    title = "Sepal length vs. width of irises"
  ) 

# Bad
ggplot(aes(x = Sepal.Width, y = Sepal.Length, color = Species)) +
  geom_point() +
  labs(x = "Sepal width, in cm", y = "Sepal length, in cm", title = "Sepal length vs. width of irises") 
```

O ggplot2 permite que você faça manipulação de dados, como filtrar ou cortar, dentro do argumento `data`.
Evite isso e faça a manipulação de dados em um pipeline antes de iniciar a plotagem.

```{r eval=FALSE}
# Good
iris %>%
  filter(Species == "setosa") %>%
  ggplot(aes(x = Sepal.Width, y = Sepal.Length)) +
  geom_point()

# Bad
ggplot(filter(iris, Species == "setosa"), aes(x = Sepal.Width, y = Sepal.Length)) +
  geom_point()
```
