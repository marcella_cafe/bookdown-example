# Arquivos {#programing}

## Nomes

Os nomes dos arquivos devem ser significativos, em inglês, minúsculo e 
terminar com .R. Caso o nome seja composto deve-se separar por um 
underscore (_).

    # Good
    conect_histvlr.R
    vallourec_card.R

    # Bad
    conect histvlr.R
    spc.R
    VallourecCard.r
    
Preste atenção à capitalização, pois você ou alguns dos colaboradores 
podem estar usando um sistema operacional com um sistema de arquivos 
que não diferencia maiúsculas de minúsculas (por exemplo, Microsoft Windows 
ou OS X), o que pode causar problemas nos sistemas de controle de revisão 
(que diferenciam maiúsculas de minúsculas). Use sempre nomes de arquivo em 
letras minúsculas e nunca tenham nomes que diferem apenas em maiúsculas.

## Estrutura Interna 

Para dividir o arquivo em seções utilize quatro `=` para comentar a linha.

```{r, eval = FALSE}
# ==== Load data ====

# ==== Plot data ====

# ==== Handling data issues ====

# ==== Omiting NA data from dataframe ====
```

Se o seu script usar pacotes complementares, carregue-os todos de uma só vez no 
início do arquivo. Isso é mais transparente do que espalhar chamadas de `library()` 
por todo o código ou ter dependências ocultas carregadas em um arquivo de 
inicialização, como `.Rprofile`.
