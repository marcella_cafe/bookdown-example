# Pipes

## Introdução

Use `%>%` para enfatizar uma sequência de ações, em vez do objeto 
no qual as ações estão sendo executadas.

Evite usar o pipe quando:

* Você precisa manipular mais de um objeto por vez. Reserve pipes para
uma sequência de etapas aplicada a um objeto principal.

* Existem objetos intermediários significativos que podem receber nomes
informativos.

## Espaço em branco

`%>%` sempre deve ter um espaço antes e geralmente deve ser seguido por 
uma nova linha. Após o primeiro passo, cada linha deve ser recuada por 
dois espaços. Essa estrutura facilita a adição de novas etapas (ou 
reorganiza as etapas existentes) e é mais difícil ignorar uma etapa.

```{r, eval = FALSE}
# Good
iris %>%
  group_by(Species) %>%
  summarize_if(is.numeric, mean) %>%
  ungroup() %>%
  gather(measure, value, -Species) %>%
  arrange(value)

# Bad
iris %>% group_by(Species) %>% summarize_all(mean) %>%
ungroup %>% gather(measure, value, -Species) %>%
arrange(value)
```

## Linhas longas

Se todos os argumentos de uma função não couberem em uma linha, coloque 
cada argumento em sua própria linha e indente:

```{r, eval = FALSE}
iris %>%
  group_by(Species) %>%
  summarise(
    Sepal.Length = mean(Sepal.Length),
    Sepal.Width = mean(Sepal.Width),
    Species = n_distinct(Species)
  )
```

## Sem argumentos

O pacote magrittr permite que você omita o `()` em funções que não possuem argumentos. Evite utilizar esse recurso.

```{r, eval = FALSE}
# Good
x %>% 
  unique() %>%
  sort()

# Bad
x %>% 
  unique %>%
  sort
```

## Atribuição

   Nome e atribuição da variável devem ser feitas na mesma linha. Quando o nome vem primeiro, ele pode atuar como um cabeçalho para lembrá-lo do objetivo do pipe.

```{r, eval = FALSE}
    iris_long <- iris %>%
      gather(measure, value, -Species) %>%
      arrange(-value)
```

O pacote magrittr fornece ao operador `%<>%` um atalho para modificar um objeto no local. Evite o uso desse operador.

```{r, eval = FALSE}
# Good
x <- x %>% 
  abs() %>% 
  sort()
  
# Bad
x %<>%
  abs() %>% 
  sort()
```
