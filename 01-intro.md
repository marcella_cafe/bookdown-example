Buscando padronizar o código desenvolvido em R, foi criado um guia de estilos derivado do Guia de Rstilo R utilizado no  [tidyverse](http://tidyverse.org).

Para facilitar o uso do Guia de Estilos é aconselhável utilizar o pacote abaixo:

*   [styler](http://styler.r-lib.org) O objetivo do styler é fornecer uma impressão bonita e não-invasiva do código-fonte R, seguindo as regras de formatação organizada e poupando assim o trabalho com espaçamentos.

# Sintaxe  {#index}

## Nome de objetos

Os nomes de variáveis e funções devem usar apenas letras minúsculas, números e `_` e ser em inglês.
Use underscore (`_`) para separar os nomes compostos.
O nomes das variáveis devem iniciar com o prefixo do seu tipo.

```{r, eval = FALSE}
# Good
df_quality
list_temp
num_temp

# Bad
quality
list
dfQuality
```
Geralmente, nomes de variáveis devem ser substantivos e nomes de funções devem ser verbos. Procure por nomes concisos e significativos.

```{r, eval = FALSE}
# Good
day_one

# Bad
first_day_of_the_month
djm1
```

Nunca utilize nomes de funções e variáveis comuns, isso pode acabar causando confusões.

```{r, eval = FALSE}
# Bad
T <- FALSE
c <- 10
mean <- function(x) sum(x)
```

## Espaçamento

### Vírgulas

Sempre coloque um espaço após uma vírgula, nunca antes.

```{r, eval = FALSE}
# Good
x[, 1]

# Bad
x[,1]
x[ ,1]
x[ , 1]
```

### Parênteses

Não coloque espaços dentro ou fora dos parênteses para chamadas de funções regulares.

```{r, eval = FALSE}
# Good
mean(x, na.rm = TRUE)

# Bad
mean (x, na.rm = TRUE)
mean( x, na.rm = TRUE )
```

Coloque um espaço antes e depois do `()` quando for utilizado com  `if`, `for`, ou `while`.

```{r, eval = FALSE}
# Good
if (debug) {
  show(x)
}

# Bad
if(debug){
  show(x)
}
```

Coloque um espaço depois do `()` usado para argumentos da função:

```{r, eval = FALSE}
# Good
function(x) {}

# Bad
function (x) {}
function(x){}
```

### Embracing

O operador embracing, `{{ }}`, deve sempre ter espaços internos para ajudar a enfatizar seu comportamento especial:

```{r, eval = FALSE}
# Good
max_by <- function(data, var, by) {
  data %>%
    group_by({{ by }}) %>%
    summarise(maximum = max({{ var }}, na.rm = TRUE))
}

# Bad
max_by <- function(data, var, by) {
  data %>%
    group_by({{by}}) %>%
    summarise(maximum = max({{var}}, na.rm = TRUE))
}
```


### Infix operators

A maioria dos operadores infix (`==`, `+`, `-`, `<-`, etc.) devem ser sempre cercados por espaços:

```{r, eval = FALSE}
# Good
height <- (feet * 12) + inches
mean(x, na.rm = TRUE)

# Bad
height<-feet*12+inches
mean(x, na.rm=TRUE)
```

Existem algumas exceções, que nunca devem ser cercadas por espaços:

*   Os operadores com [alta prioridade][syntax]: `::`, `:::`, `$`, `@`, `[`, `[[`, `^`, unário `-`, unário `+`, e `:`.
  
    ```{r, eval = FALSE}
    # Good
    sqrt(x^2 + y^2)
    df$z
    x <- 1:10

    # Bad
    sqrt(x ^ 2 + y ^ 2)
    df $ z
    x <- 1 : 10
    ```
  
*   Fórmulas de um lado quando o lado direito é um identificador único:

    ```{r, eval = FALSE}
    # Good
    ~foo
    tribble(
      ~col1, ~col2,
      "a",   "b"
    )

    # Bad
    ~ foo
    tribble(
      ~ col1, ~ col2,
      "a", "b"
    )
    ```

## Chamadas de função

### Argumentos nomeados {#argumentos-nomeados}

Sempre utilize o nome dos parâmetros nas chamadas de funções. 

Evite a correspondência parcial.

```{r, eval = FALSE}
# Good
del_out_group(column = data$target, group = data$group, type = 2)
mean(x = 1:10, na.rm = TRUE)

# Bad
del_out_group(data$target, group = data$group, type = 2)
mean(x = 1:10, , FALSE)
mean(, TRUE, x = c(1:10, NA))
```

### Atribuição

Sempre atribua um valor antes de chamar uma função.

```{r, eval = FALSE}
# Good
x <- complicated_function()
if (nzchar(x) < 1) {
  # do something
}

# Bad
if (nzchar(x <- complicated_function()) < 1) {
  # do something
}
```

## Fluxo de Controle

### Bloco de Código {#indenting}

As chaves `{}` definem a hierarquia mais importante do código R. Para facilitar a visualização dessa hierarquia:

* `{` deve ser o último caractere na linha. Código relacionado (por exemplo, uma cláusula `if`, uma declaração de função, uma vírgula à direita, ...) deve estar na mesma linha que o colchete de abertura.

* O conteúdo deve ser recuado por dois espaços.

* `}` deve ser o primeiro caractere na linha.

```{r, eval = FALSE}
# Good
if (y < 0 && debug) {
  message("y is negative")
}

if (y == 0) {
  if (x > 0) {
    log(x)
  } else {
    message("x is negative or zero")
  }
} else {
  y^x
}

test_that("call1 returns an ordered factor", {
  expect_s3_class(call1(x, y), c("factor", "ordered"))
})

tryCatch(
  {
    x <- scan()
    cat("Total: ", sum(x), "\n", sep = "")
  },
  interrupt = function(e) {
    message("Aborted by user")
  }
)

# Bad
if (y < 0 && debug) {
message("Y is negative")
}

if (y == 0)
{
    if (x > 0) {
      log(x)
    } else {
  message("x is negative or zero")
    }
} else { y ^ x }
```

### Condicional if

Se você precisar testar várias condições, coloque cada condição dentro do parêntes `()`.

```{r, eval = FALSE}
# Good
if ((value <= 0.05) & (number_groups < 2)) {
  # do something
}

# Bad
if (value <= 0.05 & number_groups < 2) {
  # do something
}
```


### Coerção implícita do tipo

Evite coerção implícita de tipo (por exemplo, de numérico para lógico) nas declarações `if`:

```{r, eval = FALSE}
# Good
if (length(x) > 0) {
  # do something
}

# Bad
if (length(x)) {
  # do something
}
```

### Switch 

* Evite declarações `switch()` baseadas em posições (ou seja, prefira nomes).
* Cada elemento deve seguir sua própria linha.
* Os elementos que se enquadram no elemento a seguir devem ter um espaço depois `=`.
* Forneça um erro de falha, a menos que você tenha validado a entrada anteriormente.

```{r, eval = FALSE}
# Good 
switch(x
  a = 0,
  b = 1, 
  c = 2,
  stop("Unknown `x`", call. = FALSE)
)

# Bad
switch(x, a = 0, b = 1, c = 2)
switch(x, a = 0, b = 1, c = 2)
switch(y, 1, 2, 3)
```

## Linhas longas

Esforce-se para limitar seu código a 80 caracteres por linha. Isso cabe confortavelmente em uma página
impressa com uma fonte de tamanho razoável. Se você ficar sem espaço, isso é uma boa indicação de que
você deve encapsular parte do trabalho em uma função separada.

Se uma chamada de função for muito longa para caber em uma única linha, use uma linha para cada nome
de função, cada argumento e o fechamento `)`. Isso facilita a leitura e a alteração do código 
posteriormente.

Use no máximo três argumentos por linhas. Caso os três argumentos seja grande, opte por colocar cada
um em uma linha.

```{r, eval = FALSE}
# Good
do_something_very_complicated(
  something = "that",
  requires = many,
  arguments = "some of which may be long"
)

# Bad
do_something_very_complicated("that", requires, many, arguments,
                              "some of which may be long"
                              )
```

## Ponto e vírgula

Não coloque `;` no final de uma linha e não use `;` para colocar vários comandos em uma linha.

## Atribuição

Use `<-`, ou `=`, de acordo com a necessidade para atribuir um valor.

```{r}
# Good
x <- 5
x = 5
```

## Dados

### Vetores de caracter

Use `"`, não `'`, para citar texto. A única exceção é quando o texto já contém aspas duplas e sem aspas simples.

```{r, eval=FALSE}
# Good
"Text"
'Text with "quotes"'
'<a href="http://style.tidyverse.org">A link</a>'

# Bad
'Text'
'Text with "double" and \'single\' quotes'
```

### Vetores lógicos

Prefira o uso de `TRUE` e `FALSE` ao invés de `T` e `F`.

## Comentários

Cada linha de um comentário deve começar com o símbolo do comentário e um único espaço: `# `

No código de análise de dados, use comentários sucintos para registrar descobertas 
importantes e decisões de análise.

Faça o comentário em inglês.
